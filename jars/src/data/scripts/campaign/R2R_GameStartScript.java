package data.scripts.campaign;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParams;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.impl.campaign.ids.Ranks;
import data.scripts.R2RModPlugin;
import exerelin.campaign.ExerelinSetupData;

public class R2R_GameStartScript {
	
	public static void execute()
	{
		if (Global.getSector().isInNewGameAdvance()) return;
		
		Global.getSector().getFaction(Factions.TRITACHYON).setRelationship(Factions.PLAYER, RepLevel.INHOSPITABLE);
		
		SectorEntityToken entity = Global.getSector().getEntityById("eochu_bres");
		MarketAPI market = entity.getMarket();
		
		Global.getLogger(R2R_GameStartScript.class).info("Eochu Bres location: " + entity.getLocation().x + ", " + entity.getLocation().y);
		Global.getSector().getPlayerFleet().setLocation(entity.getLocation().x - 300, entity.getLocation().y + 300);
		
		float combatPoints = 4;	// not too big; else it'll harry instead of chasing
		if (R2RModPlugin.hasExerelin && ExerelinSetupData.getInstance().hardMode)
			combatPoints = 5;
		
		// spawn attack fleet
		CampaignFleetAPI fleet = FleetFactoryV2.createFleet(new FleetParams(
					null,
					market, 
					market.getFactionId(),
					null, // fleet's faction, if different from above, which is also used for source market picking
					FleetTypes.PATROL_SMALL,
					combatPoints, // combatPts
					0, // freighterPts 
					0, // tankerPts
					0f, // transportPts
					0f, // linerPts
					0f, // civilianPts 
					0f, // utilityPts
					0f, // qualityBonus
					0.8f, // qualityOverride
					1f,	// officer num mult
					3)); // officer level bonus
		entity.getContainingLocation().addEntity(fleet);
		fleet.setLocation(entity.getLocation().x, entity.getLocation().y);
		fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_PATROL_FLEET, true);
		fleet.getMemoryWithoutUpdate().set(R2R_CampaignPlugin.MEM_PREFIX + "rru", true);
		fleet.getCommander().setPostId(Ranks.POST_PATROL_COMMANDER);
		fleet.getCommander().setRankId(Ranks.SPACE_COMMANDER);
		fleet.setName("RRU");
			
		fleet.clearAssignments();
		fleet.addAssignment(FleetAssignment.INTERCEPT, Global.getSector().getPlayerFleet(), 10);
		fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, entity, 1000);
	}
}
