package data.scripts.campaign;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignUIAPI;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.InteractionDialogPlugin;
import com.fs.starfarer.api.campaign.OptionPanelAPI;
import com.fs.starfarer.api.campaign.TextPanelAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.combat.EngagementResultAPI;
import com.fs.starfarer.api.impl.campaign.FleetInteractionDialogPluginImpl;
import com.fs.starfarer.api.impl.campaign.RuleBasedInteractionDialogPluginImpl;
import java.util.Map;

public class R2R_RuleBasedDialogHelper implements EveryFrameScript
{
	protected boolean isDone = false;
	protected String faction = "player_npc";
	protected String fireFirst = null;
	protected boolean fireAll = false;

	public R2R_RuleBasedDialogHelper(String fireFirst, boolean fireAll)
	{
		this.fireFirst = fireFirst;
		this.fireAll = fireAll;
	}

	@Override
	public boolean isDone()
	{
		return isDone;
	}

	@Override
	public boolean runWhilePaused()
	{
		return true;
	}

	
	@Override
	public void advance(float amount)
	{
		// Don't do anything while in a menu/dialog
		CampaignUIAPI ui = Global.getSector().getCampaignUI();
		if (Global.getSector().isInNewGameAdvance() || ui.isShowingDialog())
		{
			return;
		}

		if (!isDone)
		{
			ui.showInteractionDialog(new R2R_RuleBasedDialogHelperDialog(fireFirst, fireAll), Global.getSector().getPlayerFleet());
			isDone = true;
		}
	}
	
	protected static class R2R_RuleBasedDialogHelperDialog implements InteractionDialogPlugin
	{
		protected InteractionDialogAPI dialog;
		protected TextPanelAPI text;
		protected OptionPanelAPI options;
		protected String fireFirst;
		protected boolean fireAll;

		protected RuleBasedInteractionDialogPluginImpl optionsDialogDelegate;

		public R2R_RuleBasedDialogHelperDialog(String fireFirst, boolean fireAll)
		{
			this.fireFirst = fireFirst;
			this.fireAll = fireAll;
		}

		@Override
		public void init(InteractionDialogAPI dialog)
		{
			FleetInteractionDialogPluginImpl.inConversation = true;
			optionsDialogDelegate = new RuleBasedInteractionDialogPluginImpl();
			optionsDialogDelegate.setEmbeddedMode(true);
			optionsDialogDelegate.init(dialog);
			
			MemoryAPI mem = optionsDialogDelegate.getMemoryMap().get(MemKeys.LOCAL);
			mem.set("$specialDialog", true, 0);

			if (fireAll) fireAll(fireFirst);
			else fireBest(fireFirst);
		}

		// NOTE: we use FleetInteractionDialogPluginImpl.inConversation to tell whether we're currently delegating stuff to the RuleBasedInteractionDialogPlugin

		@Override
		public void optionSelected(String optionText, Object optionData)
		{
			if (optionData == null) return;
			if (FleetInteractionDialogPluginImpl.inConversation) {
				if (optionsDialogDelegate == null)
				{
					optionsDialogDelegate = new RuleBasedInteractionDialogPluginImpl();
					optionsDialogDelegate.setEmbeddedMode(true);
					optionsDialogDelegate.init(dialog);
				}

				optionsDialogDelegate.optionSelected(optionText, optionData);
				if (!FleetInteractionDialogPluginImpl.inConversation) {
					//optionSelected(null, Menu.INIT);
				}
				return;
			}
			else if (optionText != null) {
				text.addParagraph(optionText, Global.getSettings().getColor("buttonText"));
			}
		}

		public void fireBest(String trigger)
		{
			optionsDialogDelegate.fireBest(trigger);
		}

		public void fireAll(String trigger)
		{
			optionsDialogDelegate.fireAll(trigger);
		}

		@Override
		public void optionMousedOver(String string, Object o) {
		}

		@Override
		public void advance(float f) {
		}

		@Override
		public void backFromEngagement(EngagementResultAPI erapi) {
		}

		@Override
		public Object getContext() {
			return null;
		}

		@Override
		public Map<String, MemoryAPI> getMemoryMap() {
			return null;
		}
	}
}
