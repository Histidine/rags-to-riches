/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.scripts.campaign.events;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BattleAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CampaignUIAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.JumpPointAPI;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.events.CampaignEventTarget;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.events.BaseEventPlugin;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParams;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.util.Misc;
import data.scripts.campaign.R2R_CampaignManager;
import data.scripts.campaign.R2R_CampaignPlugin;
import data.scripts.campaign.R2R_RuleBasedDialogHelper;
import data.scripts.utils.R2R_Utils;
import java.util.List;
import org.json.JSONObject;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author Rattlesnark
 */
public class R2R_BossEvent extends BaseEventPlugin {
	
	protected PersonAPI boss = null;
	protected CampaignFleetAPI bossFleet = null;
	protected LocationAPI system = null;
	protected BossEventStage stage = BossEventStage.NONE;
	
	@Override
	public void init(String eventType, CampaignEventTarget eventTarget) {
		super.init(eventType, eventTarget, true);
		system = Global.getSector().getPlayerFleet().getContainingLocation();
		if (system == Global.getSector().getHyperspace()) system = null;
	}

	@Override
	public void setParam(Object param) {
	}
	
	public CampaignFleetAPI createBossFleet(SectorEntityToken exit)
	{
		CampaignFleetAPI playerFleet = Global.getSector().getPlayerFleet();
		int points = 0;
		for (FleetMemberAPI member : playerFleet.getFleetData().getCombatReadyMembersListCopy())
		{
			points += R2R_Utils.getHullSizePoints(member);
		}
		
		SectorEntityToken eochuBres = Global.getSector().getEntityById("eochu_bres");
		bossFleet = FleetFactoryV2.createFleet(new FleetParams(
					null,
					eochuBres.getMarket(), 
					Factions.TRITACHYON,
					null, // fleet's faction, if different from above, which is also used for source market picking
					FleetTypes.PATROL_LARGE,
					points * 0.8f, // combatPts
					points * 0.1f, // freighterPts 
					points * 0.1f, // tankerPts
					0f, // transportPts
					0f, // linerPts
					0f, // civilianPts 
					0f, // utilityPts
					0f, // qualityBonus
					0.8f, // qualityOverride
					1.25f,	// officer num mult
					5)); // officer level bonus
		
		exit.getContainingLocation().addEntity(bossFleet);
		Vector2f bossLoc = Misc.getPointAtRadius(exit.getLocation(), 500f);
		bossFleet.setLocation(bossLoc.x, bossLoc.y);
		bossFleet.getAI().addAssignmentAtStart(FleetAssignment.INTERCEPT, Global.getSector().getPlayerFleet(), 1000f, null);
		//bossFleet.getAI().addAssignmentAtStart(FleetAssignment.INTERCEPT, Global.getSector().getPlayerFleet(), 1000f, null);
		//bossFleet.getAI().addAssignmentAtStart(FleetAssignment.INTERCEPT, Global.getSector().getPlayerFleet(), 1000f, null);

		bossFleet.getMemoryWithoutUpdate().set(R2R_CampaignPlugin.MEM_PREFIX + "bossFleet", true);
		bossFleet.getMemoryWithoutUpdate().set("$exerelinFleetAggressAgainstPlayer", true);
		bossFleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_LOW_REP_IMPACT, true);
		
		boss = R2R_CampaignManager.getManager().getBoss();
		bossFleet.getFleetData().addOfficer(boss);
		bossFleet.setCommander(boss);
		bossFleet.getFlagship().setCaptain(boss);
			
		stage = BossEventStage.BOSS_SPAWNED;
		return bossFleet;
	}
	
	@Override
	public void reportBattleFinished(CampaignFleetAPI primaryWinner, BattleAPI battle) {
		//if (!battle.isPlayerInvolved()) return;
		if (bossFleet == null) return;
		if (!battle.isInvolved(bossFleet)) return;
		boolean playerInvolved = battle.isPlayerInvolved() && !battle.isOnPlayerSide(bossFleet);
		
		if (bossFleet.getFlagship() == null || bossFleet.getFlagship().getCaptain() != boss) // ded
		{
			//Global.getSector().getCampaignUI().addMessage("boss ded");
			if (playerInvolved)
			{
				Global.getSector().addScript(new R2R_RuleBasedDialogHelper("R2R_BossDefeated", false));
			}
			else
			{
				Global.getSector().addScript(new R2R_RuleBasedDialogHelper("R2R_BossDiedOther", false));
			}
			endEvent();
		}
		else if (playerInvolved) {
			Global.getSector().addScript(new R2R_RuleBasedDialogHelper("R2R_BossEscaped", false));
			endEvent();
		}
		
	}

	@Override
	public boolean allowMultipleOngoingForSameTarget() {
		return false;
	}

	@Override
	public void reportPlayerClosedMarket(MarketAPI market) {
		if (system != null) {
			stage = BossEventStage.BOSS_WANTS_TO_SPAWN;
		}
	}

	@Override
	public void reportFleetJumped(CampaignFleetAPI fleet, SectorEntityToken from, JumpPointAPI.JumpDestination to) {
		//Global.getLogger(this.getClass()).info("Fleet jumped");
		if (fleet != Global.getSector().getPlayerFleet()) return;
		//Global.getSector().getCampaignUI().addMessage("player fleet jumped");
		// TODO
		if (to.getDestination().isInHyperspace())
		{
			system = null;
			//Global.getSector().getCampaignUI().addMessage("jumping to hyper");
			if (stage == BossEventStage.BOSS_WANTS_TO_SPAWN)
			{
				createBossFleet(to.getDestination());
			}
		}
		else
		{
			//Global.getSector().getCampaignUI().addMessage("jumping to not-hyper");
			system = to.getDestination().getContainingLocation();
		}
	}
	
	@Override
	public boolean isDone() {
		//return false;
		return stage == BossEventStage.DONE;
	}
	
	public void endEvent()
	{
		R2R_CampaignManager.getManager().setStoryStage("wander2");
		stage = BossEventStage.DONE;
	}
	
	// somehow fleet never found us, forget it
	@Override
	public void reportFleetDespawned(CampaignFleetAPI fleet, FleetDespawnReason reason, Object param) {
		if (fleet == bossFleet)
		{
			endEvent();
		}
	}

	@Override
	public CampaignEventCategory getEventCategory() {
		return CampaignEventCategory.DO_NOT_SHOW_IN_MESSAGE_FILTER;
	}
	
	protected enum BossEventStage {
		NONE, BOSS_WANTS_TO_SPAWN, BOSS_SPAWNED, BOSS_RETREATING, DONE
	}
}
