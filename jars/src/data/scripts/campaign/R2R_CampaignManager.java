package data.scripts.campaign;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BaseCampaignEventListener;
import com.fs.starfarer.api.campaign.BattleAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.FullName;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.events.OfficerManagerEvent;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Ranks;
import com.fs.starfarer.api.util.IntervalUtil;
import static data.scripts.campaign.R2R_CampaignPlugin.MEM_PREFIX;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class R2R_CampaignManager extends BaseCampaignEventListener implements EveryFrameScript {
	
	public static final String PERSISTENT_DATA_KEY = "ragsToRiches";
	public static final String CHATTER_KEY = "combatChatter";
	public static final String CONFIG_PATH = "r2r_config.json";
	public static final int BOSS_EVENT_LEVEL = 25;
	
	
	public static Logger log = Global.getLogger(R2R_CampaignManager.class);
	protected static R2R_CampaignManager campaignManager;
	protected static JSONObject config;
	
	protected PersonAPI girlfriend = null;
	protected PersonAPI grandfather = null;
	protected PersonAPI boss = null;
	protected IntervalUtil interval = new IntervalUtil(0.2f, 0.2f);
	protected float daysElapsed = 0;
	protected CampaignFleetAPI playerFleet = null;
	protected FactionAPI bossKillerFaction = null;
	
	protected String storyStage = "start";
	
	public R2R_CampaignManager() {
		super(true);
	}
	
	public void loadConfig()
	{
		try {
			// load JSON data and create people
			JSONObject configJson = Global.getSettings().loadJSON(CONFIG_PATH);
			config = configJson;
		} catch (IOException | JSONException ex) {
			log.error(ex);
		}
	}

	public void initCampaign()
	{
		if (config == null) loadConfig();
		
		Global.getSector().registerPlugin(new R2R_CampaignPlugin());
		
		FactionAPI faction = Global.getSector().getFaction(Factions.INDEPENDENT);
		try {
			String gfGivenName = config.getString("girlfriendGivenName");
			String gfSurname = config.getString("girlfriendSurname");
			String gfPortrait = config.getString("girlfriendPortrait");
			String gfChatterChar = config.getString("girlfriendChatterChar");
			
			girlfriend = OfficerManagerEvent.createOfficer(faction, 1, false);
			girlfriend.getName().setFirst(gfGivenName);
			girlfriend.getName().setLast(gfSurname);
			girlfriend.getName().setGender(FullName.Gender.FEMALE);
			girlfriend.setPortraitSprite(gfPortrait);
			girlfriend.setPersonality("steady");
			girlfriend.getRelToPlayer().setRel(0.6f);
			//girlfriend.setRankId(Ranks.SPACE_ENSIGN);
			
			String grampsGivenName = config.getString("grandfatherGivenName");
			String grampsSurname = config.getString("grandfatherSurname");
			String grampsPortrait = config.getString("grandfatherPortrait");
			String grampsChatterChar = config.getString("grandfatherChatterChar");
			
			grandfather = OfficerManagerEvent.createOfficer(faction, 10, true);
			grandfather.getName().setFirst(grampsGivenName);
			grandfather.getName().setLast(grampsSurname);
			grandfather.getName().setGender(FullName.Gender.MALE);
			grandfather.setPortraitSprite(grampsPortrait);
			grandfather.getRelToPlayer().setRel(0.7f);
			grandfather.setRankId(Ranks.SPACE_COMMANDER);
			grandfather.setPersonality("cautious");
			
			String bossGivenName = config.getString("bossGivenName");
			String bossPortrait = config.getString("bossPortrait");
			
			boss = OfficerManagerEvent.createOfficer(faction, 15, true);
			boss.getName().setFirst(bossGivenName);
			boss.getName().setLast(gfSurname);
			boss.getName().setGender(FullName.Gender.MALE);
			boss.setPortraitSprite(bossPortrait);
			boss.getRelToPlayer().setRel(-0.7f);
			boss.setRankId(Ranks.SPACE_ADMIRAL);
			
			// done in startscript (to make sure it applies to Nexerelin)
			//Global.getSector().getFaction(Factions.TRITACHYON).setRelationship(Factions.PLAYER, RepLevel.INHOSPITABLE);
			
			// Combat chatter stuff
			Map<String, Object> data = Global.getSector().getPersistentData();
			Map<String, String> chatterData;
			if (data.containsKey(CHATTER_KEY))
				chatterData = (HashMap<String, String>)data.get(CHATTER_KEY);
			else {
				chatterData = new HashMap<>();
				data.put(CHATTER_KEY, chatterData);
			}
			chatterData.put(girlfriend.getId(), gfChatterChar);
			chatterData.put(grandfather.getId(), grampsChatterChar);
			data.put(CHATTER_KEY, chatterData);
			storyStage = "escape1";
			
			MemoryAPI memory = Global.getSector().getMemoryWithoutUpdate();
			memory.set(MEM_PREFIX + "stage", "escape1");
			memory.set(MEM_PREFIX + "girlfriend", girlfriend);
			memory.set(MEM_PREFIX + "girlfriendGivenName", girlfriend.getName().getFirst());
			memory.set(MEM_PREFIX + "girlfriendSurname", girlfriend.getName().getLast());
			memory.set(MEM_PREFIX + "bossGivenName", boss.getName().getFirst());
			memory.set(MEM_PREFIX + "bossSurname", boss.getName().getLast());
			memory.set(MEM_PREFIX + "bossKillerFaction", "unknown");
			
		} catch (JSONException ex) {
			log.error(ex);
		}
	}
	
	public PersonAPI getGirlfriend() {
		return girlfriend;
	}
	
	public PersonAPI getGrandfather() {
		return grandfather;
	}
	
	public PersonAPI getBoss() {
		return boss;
	}

	public FactionAPI getBossKillerFaction() {
		return bossKillerFaction;
	}

	public void setBossKillerFaction(FactionAPI faction) {
		bossKillerFaction = faction;
		Global.getSector().getMemoryWithoutUpdate().set(MEM_PREFIX + "stage", faction.getDisplayName());
	}

	@Override
	public void reportBattleFinished(CampaignFleetAPI primaryWinner, BattleAPI battle) {
		//log.info("Battle finished");
	}
	
	@Override
	public boolean isDone() {
		return false;
	}

	@Override
	public boolean runWhilePaused() {
		return false;
	}

	@Override
	public void advance(float amount) {
		if (Global.getSector().isInNewGameAdvance()) return;
		
		float days = Global.getSector().getClock().convertToDays(amount);
		daysElapsed += days;
		interval.advance(days);
		if (interval.intervalElapsed())
		{
			if (storyStage.equals("escape1"))
			{
				CampaignFleetAPI playerFleetCurr = Global.getSector().getPlayerFleet();
				if (playerFleet == null) playerFleet = playerFleetCurr;
				else if (!playerFleet.getId().equals(playerFleetCurr.getId()))	// died and respawned
				{
					Global.getSector().addScript(new R2R_RuleBasedDialogHelper("R2R_GameOver", false));
					setStoryStage("gameOver");
				}
			}
			if (storyStage.equals("wander1") && Global.getSector().getPlayerPerson().getStats().getLevel() >= BOSS_EVENT_LEVEL)
			{
				//Global.getSector().getCampaignUI().addMessage("starting boss event");
				setStoryStage("boss1");
				Global.getSector().getEventManager().startEvent(null, "r2r_boss", null);
			}
		}
	}
	
	public String getStoryStage()
	{
		//MemoryAPI memory = Global.getSector().getMemoryWithoutUpdate();
		//return (String)memory.get(MEM_PREFIX + "stage");
		return storyStage;
	}
	
	public void setStoryStage(String stage)
	{
		MemoryAPI memory = Global.getSector().getMemoryWithoutUpdate();
		memory.set(MEM_PREFIX + "stage", stage);
		storyStage = stage;
	}
	
	public static R2R_CampaignManager getManager()
	{
		if (campaignManager == null)
		{
			Map<String, Object> data = Global.getSector().getPersistentData();
			if (data.containsKey(PERSISTENT_DATA_KEY))	{
				campaignManager = (R2R_CampaignManager)data.get(PERSISTENT_DATA_KEY);
			}
			else {
				campaignManager = new R2R_CampaignManager();
				data.put(PERSISTENT_DATA_KEY, campaignManager);
			}
		}
		return campaignManager;
	}
	
}
