package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import data.scripts.campaign.R2R_CampaignManager;
import data.scripts.campaign.R2R_GameStartScript;

public class R2RModPlugin extends BaseModPlugin
{
    public static final boolean hasExerelin;

    static {
        
        boolean foundExerelin;
        try {
            Global.getSettings().getScriptClassLoader().loadClass("data.scripts.world.ExerelinGen");
            foundExerelin = true;
        } catch (ClassNotFoundException ex) {
            foundExerelin = false;
        }
        hasExerelin = foundExerelin;
    }

	@Override
	public void onNewGameAfterTimePass() {
		R2R_GameStartScript.execute();
	}
	
	@Override
	public void onNewGame() {
		R2R_CampaignManager manager = R2R_CampaignManager.getManager(); 
		Global.getSector().addScript(manager);
		manager.initCampaign();
	}
}