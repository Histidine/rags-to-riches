package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.util.Misc;
import data.scripts.R2RModPlugin;
import data.scripts.campaign.R2R_CampaignManager;
import static data.scripts.campaign.R2R_CampaignManager.log;
import exerelin.campaign.ExerelinSetupData;
import exerelin.campaign.PlayerFactionStore;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Rattlesnark
 */
public class R2R_NGCInit extends BaseCommandPlugin {
		
	@Override
	public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
		
		try {
			// load JSON data and create people
            JSONObject config = Global.getSettings().loadJSON(R2R_CampaignManager.CONFIG_PATH);
			
			if (R2RModPlugin.hasExerelin)
			{
				ExerelinSetupData setupData = ExerelinSetupData.getInstance();
				setupData.corvusMode = true;
				setupData.prismMarketPresent = config.optBoolean("nexUsePrismFreeport");
				setupData.omnifactoryPresent = config.optBoolean("nexUseOmnifactory");
				setupData.randomOmnifactoryLocation = config.optBoolean("nexRandomOmnifactoryLocation");
				setupData.respawnFactions = config.optBoolean("nexFactionsRespawn");
				setupData.onlyRespawnStartingFactions = config.optBoolean("nexOnlyStartingFactionsRespawn");
				setupData.hardMode = config.optBoolean("nexHardMode");

				//PlayerFactionStore.setPlayerFactionIdNGC(Factions.TRITACHYON);
			}
		} catch (IOException | JSONException ex) {
            log.error(ex);
        }
		return true;
	}
}