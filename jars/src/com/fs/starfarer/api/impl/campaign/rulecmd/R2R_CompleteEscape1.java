package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.util.Misc;
import data.scripts.campaign.R2R_CampaignManager;
import data.scripts.campaign.R2R_CampaignPlugin;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Rattlesnark
 */
public class R2R_CompleteEscape1 extends BaseCommandPlugin {
		
	@Override
	public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
		
		CampaignFleetAPI fleet = Global.getSector().getPlayerFleet();
		fleet.getCargo().removeCrew(CargoAPI.CrewXPLevel.GREEN, 1);
		fleet.getCargo().removeCrew(CargoAPI.CrewXPLevel.ELITE, 1);
		
		//Global.getSector().adjustPlayerReputation(
		//					new CoreReputationPlugin.RepActionEnvelope(CoreReputationPlugin.RepActions.MAKE_SUSPICOUS_AT_WORST, 0),
		//					Factions.TRITACHYON);
		
		R2R_CampaignManager manager = R2R_CampaignManager.getManager();
		PersonAPI grandfather = manager.getGrandfather();
		PersonAPI girlfriend = manager.getGirlfriend();
		fleet.getFleetData().addOfficer(grandfather);
		fleet.getFleetData().addOfficer(girlfriend);
		
		MemoryAPI memory = memoryMap.get(MemKeys.LOCAL);
		memory.set(R2R_CampaignPlugin.MEM_PREFIX + "girlfriendFullName", girlfriend.getName().getFullName(), 0);
		memory.set(R2R_CampaignPlugin.MEM_PREFIX + "grandfatherFullName", grandfather.getName().getFullName(), 0);
		memory = memoryMap.get(MemKeys.GLOBAL);
		memory.set(R2R_CampaignPlugin.MEM_PREFIX + "escapeComplete", true);
		manager.setStoryStage("wander1");
		
		return true;
	}
}