package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.SectorEntityToken.VisibilityLevel;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.util.Misc;
import data.scripts.campaign.R2R_CampaignManager;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Rattlesnark
 */
public class R2R_CanTradeAtMarket extends BaseCommandPlugin {
		
	@Override
	public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
		
		SectorEntityToken interactionTarget = dialog.getInteractionTarget();
		MarketAPI market = interactionTarget.getMarket();
		if (market == null) return false;
		
		CampaignFleetAPI fleet = Global.getSector().getPlayerFleet();
		if (!fleet.isTransponderOn())	// sneaking in
		{
			if (!market.getFaction().getCustomBoolean(Factions.CUSTOM_ALLOWS_TRANSPONDER_OFF_TRADE) && !market.hasCondition(Conditions.FREE_PORT))
			{
				if (isSeenByPatrols(market.getFactionId()))
					return false;
			}
		}
		else
		{
			// they won't let us in if they don't like us
			if (market.getFaction().isAtBest(Factions.PLAYER, RepLevel.INHOSPITABLE))
				return false;
		}
		return true;
	}
	
	// taken from IsSeenByPatrols.java
	public boolean isSeenByPatrols(String factionId) {
		
		CampaignFleetAPI playerFleet = Global.getSector().getPlayerFleet();

		for (CampaignFleetAPI fleet : playerFleet.getContainingLocation().getFleets()) {
			if (!fleet.getFaction().getId().equals(factionId)) continue;
			
			VisibilityLevel level = playerFleet.getVisibilityLevelTo(fleet);
			MemoryAPI mem = fleet.getMemoryWithoutUpdate();
			if (!mem.contains(MemFlags.MEMORY_KEY_SAW_PLAYER_WITH_TRANSPONDER_OFF)) {
				if (level == VisibilityLevel.NONE) continue;
			}
			
			if (isPatrol(fleet)) return true;
		}
		return false;
	}
	
	private boolean isPatrol(CampaignFleetAPI fleet) {
		if (!fleet.getMemoryWithoutUpdate().contains(MemFlags.MEMORY_KEY_PATROL_FLEET)) {
			return false;
		}
		
		for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
			if (!member.isCivilian()) return true;
		}
		
		return false;
	}
}