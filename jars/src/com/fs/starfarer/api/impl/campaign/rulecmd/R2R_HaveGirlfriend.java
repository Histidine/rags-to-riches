package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.OfficerDataAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.util.Misc;
import data.scripts.campaign.R2R_CampaignManager;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Rattlesnark
 */
public class R2R_HaveGirlfriend extends BaseCommandPlugin {
		
	@Override
	public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
		R2R_CampaignManager manager = R2R_CampaignManager.getManager();
		PersonAPI girlfriend = manager.getGirlfriend();
		List<OfficerDataAPI> officers = Global.getSector().getPlayerFleet().getFleetData().getOfficersCopy();
		for (OfficerDataAPI officerData : officers)
		{
			if (officerData.getPerson().getId().equals(girlfriend.getId()))
				return true;
		}
		return false;
		//return Global.getSector().getPlayerFleet().getFleetData().getOfficerData(manager.getGirlfriend()) != null;
	}
}