package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.util.Misc;
import data.scripts.campaign.R2R_CampaignManager;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Rattlesnark
 */
public class R2R_IsGirlfriendAssigned extends BaseCommandPlugin {
		
	@Override
	public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
		R2R_CampaignManager manager = R2R_CampaignManager.getManager();
		PersonAPI girlfriend = manager.getGirlfriend();
		for (FleetMemberAPI member : Global.getSector().getPlayerFleet().getFleetData().getMembersListCopy())
		{
			if (member.getCaptain().getId().equals(girlfriend.getId()))
				return true;
		}
		return false;
	}
}